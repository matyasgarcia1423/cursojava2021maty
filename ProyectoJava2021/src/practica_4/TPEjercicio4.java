package practica_4;

import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

public class TPEjercicio4 {

	public static void main(String[] args) {
		System.out.println("Categorias disponibles: a,b y c");
		System.out.println("a = 1, b = 2, c = 3");
		System.out.println("Ingresar una categoria");
		Scanner scan = new Scanner(System.in);
		byte z = scan.nextByte();
		
		if (z == 1)
			System.out.println("Hijo");
		if (z == 2)
			System.out.println("Padres");
		if (z == 3)
			System.out.println("Abuelos");
		}
	}
	