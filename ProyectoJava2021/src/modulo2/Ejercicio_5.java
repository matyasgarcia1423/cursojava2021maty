package modulo2;

public class Ejercicio_5 {

	public static void main(String[] args) {
		//5-	Cual de las siguientes líneas dan errores de compilación y para esos casos cubrirlos con el casteo correspondiente
		//I = s;	No hay error de compilacion.
		//b=s;	Error de compilacion		short numero_de_short = s;
		//l=i;	No hay error de compilacion			
		//b=i;	Hay error de compilacion		int numero_de_int = i;
		//s=i;	Hay error de compilacion		int numero_de_int = i;
		
	}

}
