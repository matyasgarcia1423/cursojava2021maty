package modulo2;

public class Mod2_Ejercicio2 {

	public static void main(String[] args) {
		byte 		bmin = -128;
		byte 		bmax = 127;
		short 	smin = -32768;
		short 	smax = 32767;
		int 		imax = 2147483647;
		int 		imin = -2147483648;
		long 		lmin = -9223372036854775808L;
		long 		lmax =  9223372036854775807L;
		System.out.println("tipo\tminimo\t\t\t\tmaximo");
		
		System.out.println("\nbyte\t" + bmin + "\t\t\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t\t\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t\t" + lmax);



		
	}

}
